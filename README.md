# Guest Info Integration

This repo contains deployments of the Guest Info System.



## Overview

## Status

Code ready to integrate systems when ready. All previous progress has been abandoned for [icon](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration/-/issues/1#what-was-the-outcome) or [halted due to project readiness](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration/-/issues/3#what-was-the-outcome), the frontend and backend has been [updated](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration/-/commit/f7d86ba224bf3f28f0762ac73ff702dd4dc9e504) to the latest builds as of 4 months ago.

## Installation Instructions

### Windows

Stand alone deployment for Windows. This will provide a console to run the guest info system on a Windows system with Docker       running. You can run the console by launching `run.cmd` in the same folder as `docker-compose.yaml`.

### Other

TBD

## Usage Instructions

TBD

## Tools

### Docker

### Database System

## License

By submitting this issue or commenting on this issue, or contributing any content to this issue, you certify under the Developer Certificate of Origin that the content you post may be licensed under GPLv3 (for code) or CC-BY-SA 4.0 International (for non-code content).
